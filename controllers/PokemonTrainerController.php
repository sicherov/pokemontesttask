<?php

class PokemonTrainerController
{

    public function actionAddNewPokemon($pokemon = "")
    {
        if(!$pokemon){
            echo "Необходимо ввести название покемона!" . PHP_EOL;
            return true;
        }
        $data  = PokemonModel::storePokemonData($pokemon);
        if($data){
            echo "Покемон успешно добавлен!" . PHP_EOL;
        }
        return $data;
    }

    public function actionGetPokemonList()
    {
        $pokemonList = PokemonRepository::getAllPokemons();
        foreach ($pokemonList as $pokemon){
            echo $pokemon->getId() . " - " . $pokemon->getName() . PHP_EOL;
        }
        return true;
    }

    public function actionInstall()
    {
        Db::install();
        echo "Установка завершена" . PHP_EOL;
        return true;
    }
}
