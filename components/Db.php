<?php

/**
 * Class Db
 */
class Db
{
    /**
     * @var PDO подключение к базе данных
     */
    private static $db;

    /**
     * @var array список возможных направлений сортировок для запросов
     */
    static $sortOrder = array("asc","desc");

    /**
     * Подключение к базе данных
     *
     * @return PDO класс подключения к базе данных
     */
    public static function getConnection()
    {
        if(!self::$db) {
            $paramsPath = ROOT . '/config/db_params.php';
            $params = include($paramsPath);


            $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
            $db = new PDO($dsn, $params['user'], $params['password']);
            $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $db->exec("set names utf8");
            self::$db = $db;
        }

        return self::$db;
    }

    /**
     * Установка базы данных
     * @return mixed
     */
    public static function install(){

        $db = self::getConnection();
        $sql = file_get_contents(ROOT."/installFiles/db.sql");
        $result = $db->query($sql);

        return $result->fetch();
    }
}
