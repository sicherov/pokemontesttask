<?php

class Router
{

    private $routes;
    private $argv;

    public function __construct($argv)
    {
        $routesPath = ROOT . '/config/routes.php';
        $this->routes = include($routesPath);
        array_shift($argv);
        $this->argv = $argv;
    }

    /**
     * Returns request string
     */
    static function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            $uri = explode("?",$_SERVER['REQUEST_URI']);
            return trim($uri[0], '/');
        }
    }

    function getArgs()
    {
        return $this->argv;
    }
    public function run()
    {

        $argv = $this->getArgs();

        foreach ($this->routes as $uriPattern => $path) {

            if ($uriPattern == $argv[0]) {
                PokemonRepository::load();
                // Получаем внутренний путь из внешнего согласно правилу.
                $internalRoute = preg_replace("~$uriPattern~", $path, $argv[0]);
                // Определить контроллер, action, параметры
                $segments = explode('/', $internalRoute);

                $controllerName = array_shift($segments) . 'Controller';
                $controllerName = ucfirst($controllerName);

                $actionName = 'action' . ucfirst(array_shift($segments));


                array_shift($argv);

                // Создать объект, вызвать метод (т.е. action)
                $controllerObject = new $controllerName;

                $result = call_user_func_array(array($controllerObject, $actionName), $argv);


                if ($result != null) {
                    exit();
                }
            }
        }
    }

}
