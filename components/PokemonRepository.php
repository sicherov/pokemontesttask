<?php

class PokemonRepository
{

    static $pokemonList = array();

    /**
     * @return PokemonData[]
     */
    public static function getAllPokemons()
    {
        return self::$pokemonList;
    }

    public static function load()
    {
        PokemonModel::loadPokemons();
    }


}
