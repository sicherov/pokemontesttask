<?php

class PokemonModel{

    private static function checkPokemon($pokemon)
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM pokemons where name=:name';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $pokemon, PDO::PARAM_STR);

        $result->execute();

        if ($result->fetchColumn())
            return true;
        return false;
    }


    public static function storePokemonData($pokemon)
    {
        $db = Db::getConnection();
        $sql = 'INSERT INTO pokemons (name) '
            . 'VALUES (:name)';

        $result = $db->prepare($sql);
        $result->bindParam(':name', $pokemon, PDO::PARAM_STR);
        $result = $result->execute();
        if($result){
            self::addPokemon($db->lastInsertId("pokemons"),$pokemon);
        }
        return $result;
    }

    private static function addPokemon($id,$name)
    {
        $data = new PokemonData();
        $data->setId($id)->setName($name);
        PokemonRepository::$pokemonList[] = $data;
    }

    public static function loadPokemons()
    {
        $db = Db::getConnection();
        $sql = 'SELECT * FROM pokemons';

        $result = $db->prepare($sql);
        $result->execute();

        while ($pokemon = $result->fetch()){
            self::addPokemon($pokemon["id"],$pokemon["name"]);
        }
    }
}
