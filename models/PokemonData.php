<?php
/**
 * Created by PhpStorm.
 * User: Sicher
 * Date: 30.12.2019
 * Time: 3:49
 */

class PokemonData
{
    private $id;
    private $name;

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param mixed $id
     * @return PokemonData
     */
    public function setId($id)
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     * @return PokemonData
     */
    public function setName($name)
    {
        $this->name = $name;
        return $this;
    }


}
