Для начала необходимо настроить базу данных mysql.

_Настройки в_ `config/db_params.php`

Для установки базы данных необходимо выполнить команду install

_Пример_

`C:\OSPanel\modules\php\PHP-7.2-x64\php.exe E:\cli_pokemon_script\index.php install`

_Доступные команды:_

`addNewPokemon pokemonName`

`getPokemonList`


